require 'spec_helper'

describe 'Card' do
  describe 'Check cards' do
    context 'When card is right' do
      let(:card_number) { VALID_NUMBERS.sample }

      it 'should returns `true` for valid numbers' do
        expect(Card.is_valid?(card_number)).to eq true
      end
    end

    context 'When card is invalid' do
      let(:card_number) { INVALID_NUMBERS.sample }

      it 'should returns `false` for invalid numbers' do
        expect(Card.is_valid?(card_number)).to eq false
      end
    end
  end

  describe 'Cards calculation' do
    let(:card_number) { INVALID_NUMBERS.sample }
    let(:index) { INVALID_NUMBERS.find_index(card_number) }

    it 'should return original number' do
      expect(Card.calc_original_numb(card_number)).to eq VALID_NUMBERS[index]
    end
  end
end