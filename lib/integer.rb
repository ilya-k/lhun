class Integer
  def int_to_a
    arr = []
    tmp = self
    while tmp > 0
      arr << tmp % 10
      tmp /= 10
    end
    arr.reverse
  end
end