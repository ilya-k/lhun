require 'integer'

class Card
  class << self
    def is_valid?(card_number)
      checksum(card_number) == 0
    end

    def calc_original_numb(partial_card_number)
      card_number = partial_card_number * 10
      check_digit = checksum(card_number)
      card_number += check_digit.zero? ? check_digit : (10 - check_digit)
      card_number
    end

    private

    def get_digits(card_number)
      numbers = card_number.int_to_a.reverse
      numbers.map.with_index do |n, i|
        if i.odd?
          t = n * 2
          t > 9 ? t.int_to_a.inject(:+) : t
        end || n
      end
    end

    def sum_of_digits(card_number)
      get_digits(card_number).inject(:+)
    end

    def checksum(card_number)
      sum_of_digits(card_number) % 10
    end
  end
end